import React, { Component } from 'react';
import axios from 'axios';


 class AddProduct extends Component{
    
    constructor(props){
        super(props);
        this.state = {
            productName:"",
            price:""
        }
        this.handleSubmit=this.handleSubmit.bind(this);
        this.handleChangeName=this.handleChangeName.bind(this);
        this.handleChangePrice=this.handleChangePrice.bind(this);
    }
   
    handleChangeName(e){
          this.setState({productName:e.target.value})
    }
    handleChangePrice(e){
          this.setState({price:e.target.value})
    }
    handleSubmit(e){
        e.preventDefault();
        let product = {
            productName:this.state.productName,
            price:this.state.price
        }
        axios.post('https://homework2-dragosneacsu3.c9users.io:8080/add',product)
        .then((res) => {
            if(res.status == 200){
                this.props.productAdd(res.data);
                console.log(res.data);
            }
            }).catch((Err)=>{
                console.log(Err);
            });
                
     }
        
   render() {
      
    return (
      <div className = "AddProduct">
        <form className = "formAddProd" onSubmit={this.handleSubmit}>
    
            <input type="text" placeholder="ProductName" onChange={this.handleChangeName}/>
            <input type="text" placeholder="Price" onChange={this.handleChangePrice}/>
            <button type="submit"> Add</button>
        </form>
      </div>
    );
  }
}

export default AddProduct;
