import React, { Component } from 'react';
import axios from 'axios';

class ProductList extends Component {
    
    
    constructor(props){
        super(props);
    }
   render() {
       let produse = this.props.productList.map((product, index) =>{
            return <div key={index}>{product.productName}---{product.price}</div>
       });
    return (
      <div className="listProd">
       {produse}
      </div>
    );
  }
}

export default ProductList;