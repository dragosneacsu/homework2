import React, { Component } from 'react';
import './App.css';
import ProductList from './components/ProductList';
import AddProduct from './components/AddProduct';
import axios from 'axios';
class App extends Component {
  
  constructor(props){
    super(props);
     this.state={};
    this.state.productList=[];
  }
  onProductAdded = (product) =>{
      let products = this.state.productList;
      products.push(product);
      this.setState({productList:products});
  }
  componentWillMount(){
        axios.get('https://homework2-dragosneacsu3.c9users.io:8080/get-all')
        .then((res) => {
            this.setState({productList:res.data})
        }).catch((err) =>{
            console.log(err)
        });
        }
  render() {
    return (
      <div className="App">
         <AddProduct productAdd= {this.onProductAdded}/>
         <ProductList productList={this.state.productList}/>
      </div>
    );
  }
}

export default App;
