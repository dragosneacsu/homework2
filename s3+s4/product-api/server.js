const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})
app.put('/update/:id', (req,res) =>{
    
    
    if(req.body.productName && req.body.price){
        

    let found = false;
    for (let v = 0;v< products.length;v++){
        if(products[v].id == req.params.id){
            found = true;
            products[v].price = req.body.price;
            products[v].productName = req.body.productName;
            res.status(200).send(products[v]);
        }
        }
        if(found == false){
            res.status(404).send("Nu exista produsul!!");
        }
    }
    else res.status(500).send("Invalid body!");
});
app.delete('/delete', (req,res) =>{
   // utilizam payload ca si name
    if(req.body.payload){
        let found = false;
        for ( let v= 0;v<products.length;v++){
            if(products[v].productName == req.body.payload){
                found = true;
                products.splice(it,1);
                res.status(200).send("Stergere cu succes!");
            }
        }
        if(found == false){
             res.status(404).send("Nu exista produsul!!");
        }
    }
    else {
        res.status(500).send("Invalid payload!");
    }
});
app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.listen(8080, () => {
    console.log('Server started on port 8080...');
});